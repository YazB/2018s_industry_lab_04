package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    int count;
    char symbol;

    public Pattern(int count, char symbol) {
        this.count = count;
        this.symbol = symbol;
    }

    public int getNumberOfCharacters() {
        return count;
    }

    public void setNumberOfCharacters(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        String line = "";

        for (int i = 0; i < count; i++) {
            line += symbol;
        }

        return line;
    }
}

//}
//public void start() {
//    PrintPatternOne();
//}
//    private void printPatternOne() {
//        System.out.println("First Pattern");
//
//    Pattern top = new Pattern(15, '*');
//
//    Pattern sideOfFirstLine = new Pattern(7, '#');
//    Pattern middle1=new Pattern(1, '.');
//    Pattern sideOfSecondLine= new Pattern(7,'#');
//    Pattern sideOfLine = new Pattern(7, '~');
//    Pattern middle = new Pattern(1, '.');
//
//        System.out.println(top);
//        System.out.println(sideOfFirstLine.toString() + middle.toString() + sideOfFirstLine.toString());
//
//        for (int i = 0; i < 6; i++) {
//        middle.setNumberOfCharacters(middle.getNumberOfCharacters() + 1);
//        System.out.println(sideOfLine.toString() + middle.toString() + sideOfLine.toString());
//    }
//}
//}
