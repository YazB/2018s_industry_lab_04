package ictgradschool.industry.arrays.deodorant;

public class Deodorant {

    private String brand;
    private String fragrance;
    private boolean rollOn;
    private double price;

    public Deodorant(String brand, String fragrance,
                     boolean rollOn, double price) {

        this.brand = brand;
        this.fragrance = fragrance;
        this.rollOn = rollOn;
        this.price = price;
    }

    public String toString() {
        String info = brand + " " + fragrance;
        if (rollOn) {
            info = info + " Roll-On";
        } else {
            info = info + " Spray";
        }
        info +=  " Deodorant, \n$" + price;
        return info;
    }

    // TODO Implement all methods below this line.

    public double getPrice() {
        return this.price;
    }

    public String getBrand() {
        return this.brand;
    }

    public boolean isRollOn() {

        return this.rollOn;
    }

    public String getFragrance() {
       return this.fragrance;
    }

    public void setPrice(double pri) {
    price=pri;
    }

    public void setBrand(String bra) {
    brand=bra;
    }

    public void setFragrance(String fra) {
    fragrance=fra;
    }

    public boolean isMoreExpensiveThan(Deodorant other) {
        return this.price>other.price;
    }
}